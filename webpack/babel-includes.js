const path = require('path')

var babelIncludes = [
  new RegExp(path.join(__dirname, '../node_modules/pubsweet-client/src')),
  new RegExp(path.join(__dirname, '../app')),
  new RegExp(path.join(__dirname, '../node_modules/pubsweet-.*')),
  new RegExp(path.join(__dirname, '../node_modules/editoria-.*'))
]

module.exports = babelIncludes
